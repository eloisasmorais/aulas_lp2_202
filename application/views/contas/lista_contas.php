<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto border mt-5 pt-3 pb-3">
            <form method="POST" id="contas-form">
                
                <input name="parceiro" class="form-control" type="text" placeholder="Devedor / Credor"><br>
                <input name="descricao" class="form-control" type="text" placeholder="Descrição"><br>
                <input name="valor" class="form-control" type="number" placeholder="Valor"><br><br>
                <input type="hidder" name="tipo"value="<?= $tipo ?>">

                <div class="text-center text-md-left">
                    <a class="btn btn-primary" onclick="document.getElementById('contas-form').submit();">Cadastrar conta</a>                
                </div>
            </form>
        </div>
    </div>
</div>